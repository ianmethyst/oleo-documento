source := includes
sources := $(sort $(wildcard $(source)/*.md))
template := template/template.latex
output := tp.pdf

$(output): $(sources) $(template)
	pandoc \
		--template $(template) \
		-o $@ $(sources)

.PHONY: watch
watch: 
	    while true; do make --silent; sleep 1; done  

clean:
	rm -f $(output)
