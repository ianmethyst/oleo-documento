﻿
# Renderizados de los modelos empleados

A continuación se presentan dos renderizados de los modelos 3D empleados, que fueron escaneados de las piezas reales de las que disponíamos.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.80\textwidth]{./img/voltaire_render.png}
  \caption{Renderizado del modelo 3D del busto de Voltaire}
  \label{fig:volt_ren}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.80\textwidth]{./img/pierna_render.png}
  \caption{Renderizado de la pierna de Antinoo}
  \label{fig:pier_ren}
\end{figure}
