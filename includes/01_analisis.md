
# Análisis de las piezas, propiedades, historia

## Busto de Voltaire

El busto de Voltaire es obra de Jean-Antoine Houdon (1741 - 1828), un escultor francés del neoclásico que realizó trabajos de este estilo para un gran número de figuras relevantes de su época (como Rousseau, Diderot, Washington y otros).

\begin{figure}[H]
  \centering
  \includegraphics[width=0.33\textwidth]{./img/houdon.jpg}
  \caption{Retrato de Houdon pintado por Rembrandt Piele (1808)}
  \label{fig:houdon_piele}
\end{figure}

La escultura neoclásica apuntaba a idealizar a sus modelos, buscando la claridad de líneas y la "sencillez", que los artistas intentaron imitar del abundante arte clásico. Siguiendo con esto, las esculturas solían ser de personajes tomados de la mitología greco-romana.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.33\textwidth]{./img/jason_thorvaldsen.jpg}
  \caption{Jasón con el Vellocino de Oro de Bertel Thorvaldsen (1803)}
  \label{fig:jason}
\end{figure}

 Houdon, sin embargo, ya influenciado en parte por el Romanticismo, que comenzaba a hacerse lugar, buscó ser un poco más fiel para con cada individuo, aspirando a transmitir la complejidad de la expresión humana. El busto de Voltaire que se nos asignó para el trabajo es así una de las tantas reproducciones de los de Houdon, y se puede ver en él una sonrisa socarrona propia del filósofo y del ingenio por el cual aún hoy se lo reconoce, así también como las "arrugas" que demarcan su vejez y fragilidad.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.33\textwidth]{./img/voltaire_houdon.jpg}
  \caption{Otro busto de Voltaire perteneciente a las series de Houdon}
  \label{fig:voltaire_houdon}
\end{figure}

## Pierna de "Antinoo"

Por otro lado, contamos con otra obra, la cual analizando de forma superficial, podríamos decir que consta de una pierna izquierda, tallada, y, al lado, un bloque indefinido; ambos apoyados en una superficie que hace las veces de "suelo".

\begin{figure}[H]
  \centering
  \includegraphics[width=0.33\textwidth]{./img/pierna.jpg}
  \caption{Pierna de Antinoo}
  \label{fig:pierna_antinoo}
\end{figure}

En un inicio, contábamos con poca información sobe la pieza. Lejos de creer que la falta de información constituía un problema, lo asumimos como una fortaleza. Argüimos que esto nos daría la libertad de integrar a la pieza en función del busto, del cual comenzamos a tener una idea clara y definida.

El Renacimiento trajo consigo un pasaje de un teocentrismo al antropocentrismo, y así, en las que fueron nuestras primeras sesiones de *brainstorming*, nos figuramos a la pierna en relación con la metáfora de "el humano construyéndose a sí mismo": un intento de construcción del ser.

Más tarde, nos dieron información respecto a la pieza: la nomenclatura "pierna de Antinoo" indicaba que se trataba de una porción de una escultura de Antinoo. Luego de indagar, descubrímos que se hicieron una gran cantidad de obras respecto a esta figura histórica. Así también, encontramos que había sido excavada una en Delfos, Grecia. Esto nos permitió darnos una idea de cómo debía ser, parcialmente, la estatua completa.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.33\textwidth]{./img/antinoo.jpg}
  \caption{Pierna de Antinoo}
  \label{fig:antinoo}
\end{figure}
