
# Descripción de la idea

La instalación se compone a partir de diferentes módulos que guardan tanto una relación entre sí, como con el abordaje conceptual del que partimos. Éstos son:

- **Mesa**: Es donde se brinda la posibilidad de dar una entrada al sistema: al mover los objetos sobre su superficie y a partir de la disposición de éstos, se pueden resolver los diferentes puzzles que disparan animaciones en el resto de los módulos.
- **STD**[^std]: Sobre la mesa hay un sostenedor para celulares. En la instalación también hay una red WiFi, que al acceder redirige el navegador a la webapp. Ésta hace uso de la cámara del dispositivo para mostrar lo que se ve a través del celular, y proyecta sobre la pierna de Antinoo el _eidolon renacentista_.
- **SAR**[^sar]: A medida que se resuelven los puzzles, partículas aparecen proyectadas sobre el bloque que acomapaña la pierna, que sirve al _eidolon renacentista_ como "pintura", y que usa para colorear el rostro de Voltaire.

[^std]: i.e See-Through Display. Traducido de Wikipedia en inglés: "Una see-through display es una pantalla electrónica que permite ver lo que está en la pantalla a la vez que permite ver a través de ésta". Ver más en [https://en.wikipedia.org/wiki/See-through_display](https://en.wikipedia.org/wiki/See-through_display)

[^sar]: i.e Spatial Augmented Reality. Según Wikipedia en Español: "La realidad aumentada espacial (SAR) hace uso de proyectores digitales para mostrar información gráfica sobre los objetos físicos". Ver más en [https://es.wikipedia.org/wiki/Realidad_aumentada#Proyecci%C3%B3n_espacial](https://es.wikipedia.org/wiki/Realidad_aumentada#Proyecci%C3%B3n_espacial)

## Mesa y sistema de puzzles

La parte de la experiencia donde transcurre la interacción en sí se da con la interfaz física de la mesa que fabricamos, que con el software ReacTIVision[^reactivision] y el uso de marcadores fiduciarios[^fiduciario] permite el seguimiento espacial de objetos. Encima de la mesa, sobre una superficie de vidrio, se encuentran una serie de semiesferas que en sus bases poseen marcadores únicos para localizarlas. Al disponerlas de diferentes formas y "resolver" una serie de *puzzles*, se disparan las animaciones. Éstos, en sí, corresponden a tres tipos de disposiciones especiales para las piezas.

Los tres ideales de la Revolución Francesa ("Libertad, Igualdad y Fraternidad") se ven representados mediante las diferentes disposiciones, y cada vez que se "resuelve" uno de los puzzles, se ilumina uno de los tres leds con un color específico, según los siguientes colores:

- Rojo, por *fraternité* (fraternidad). 
- Azul, por *liberté* (libertad). 
- Blanco, por *égalité* (igualdad). 

Por supuesto, no es coincidencia que correspondan con los colores de la bandera de Francia. Si bien hay diversas explicaciones que definen el porqué de cada uno, elegimos emplear esta asociación popular por adaptarse mejor a las ideas que buscamos representar.

Las soluciones a los diferentes puzzles son:

- Fraternidad: Acercar todas las semiesferas entre sí.
- Libertad: Alejar todas las semiesferas entre sí.
- Igualdad: Alinear todas las semiesferas de forma que queden dispuestas en una línea recta horizontal.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.50\textwidth]{./img/help.png}
  \caption{Ilustración de la patanlla de ayuda explicando como usar la mesa}
  \label{fig:mesa}
\end{figure}

[^fiduciario]: Según Wikipedia en español: "Un marcador de referencia o fiduciario es un objeto utilizado para la observación de sistemas de imágenes, el cual aparece en la imagen para ser usado como punto de referencia o de medida. Además puede ser ubicado como una marca o grupo de marcas en un instrumento óptico.". Ver más en: [https://es.wikipedia.org/wiki/Marcador_de_referencia](https://es.wikipedia.org/wiki/Marcador_de_referencia)

[^reactivision]: Traducido de su sitio web oficial en inglés: reacTIVision es un _framework_ multiplataforma y de código abierto de _Computer Vision_ para el seguimiento rápido y robusto de marcadores fiduciario pegados a objetos físicos...". Ver más en [https://github.com/mkalten/reacTIVision](https://github.com/mkalten/reacTIVision)

## See-through display

En realidad, la experiencia comienza en los dispositivos móviles. Una vez se haya ingresado a la red red WiFi y accedido a la webapp, aparece sobre el _feed_ de la cámara la siguiente imagen: 

\begin{figure}[H]
  \centering
  \includegraphics[width=0.40\textwidth]{./img/frame.png}
  \caption{Imagen de referencia para encuadre}
  \label{fig:frame}
\end{figure}

Esta imagen indica que se debe encuadrar las piezas que estarán en la pared frente a la mesa. Una vez hecho esto, la imagen desaparece y se comienza a visualizar el _ediolón renacentista_ que extiende la pierna de Antinoo. Además, sobre este marco hay un botón con un signo de interrogación (?), que al pulsarlo despliega una pantalla de ayuda, explicando brevemente cómo hacer uso de la instalación.

En un primer momento, el _Eidolón_ es muy translúcido y sólo está respirando. A partir del momento en el que se completa un _puzzle_, se vuelve más opaco y comienza a pintar (o, en el caso de que se haya llegado al final, contemplar el busto de Voltaire).

La pintura colgada junto a Voltaire opera como marcador para localizar espacialmente al _Eidolón_, además de cimentar el concepto de la obra.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.80\textwidth]{./img/libertad_guiando.jpg}
  \caption{``La libertad guiando al pueblo''}
  \label{fig:lib}
\end{figure}


\begin{longtable}{ | l | l |}
  \hline                       
Autor        & Eugène Delacroix							    \\
Año					 & 1830                             \\
Técnica      & Óleo sobre lienzo                \\
Estilo       & Romanticismo                     \\
Tamaño       & 260cm × 325cm                    \\
Localización & Museo del Louvre, Paris, Francia \\
  \hline  
\end{longtable}


Por otro lado, estamos conscientes del hecho de que la pintura pertenece al período que sucede al Neoclacisimo, pero por su temática, creemos que guarda una relación clara con los conceptos trabajados en la instalación.

## Realidad aumentada espacial

En la medida que se complete cada _puzzle_ y que el _eidolon_ se materialice, a través de la proyección se visualizan partículas de uno de los tres colores de la bandera francesa (el que corresponda con el último _puzzle_ resuelto) dentro del bloque que está junto a la pierna. Una vez el _eidolon_ (en el STD) esté opaco, él moverá su pincel hasta el bloque, donde las partículas desaparecerán al pasárselo. Luego de esto, él pinta un tercio de cara de Voltaire con este color, pero esto ocurre en la proyección. En este punto, no hay una relación entre la morfología de la cara y lo que se está proyectando. Parece totalmente arbitrario, pero una vez que el _eidolon_ termina de pintar, él desaparece y la pintura que era de un único color se licúa y pasa a tener colores y forma que se adecúan a los de los retratos de Voltaire: la cara que carecía de vida ahora pasa a tener color.

Esto sucede tres veces, hasta que la cara se encuentre completamente pintada. Durante el final de la experiencia, la pintura se licúa volviendo al estado original de la obra.

## Final de la experiencia

Cuando se resuelven los tres puzzles (se puede saber al observar que los tres leds están encendidos y que el busto de Voltaire está completamente "pintado"), el _eidolon renacentista_ aparece una última vez, y asume una posición reflexiva mientras contempla "su obra". Segundos después, él comienza a solidificarse, dejando su aspecto fantasmagórico para pasar a ser parte de la escultura que extendía. Finalmente, comienza a quebrajarse y se desarma en pedazos a la vez que la pintura del busto de Voltaire se "licúa". Mientras esto pasa, los leds de la mesa se apagan y la experiencia se reinicia.

Así, la experiencia habla del pasaje de renacimiento al modernismo, en tanto hay una retroalimentación entre el las ideas de la revolución francesa y el _eidolón_, que finalmente concluye en el declive de las ideas del modernismo.
