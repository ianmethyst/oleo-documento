---
subject: Lenguaje Multimedial III
title: Trabajo Práctico 5
subtitle: Realidad aumentada
university: Universidad Nacional de La Plata
faculty: Facultad de Bellas Artes
year: 2018
documentclass: scrartcl
romankomafont: true
author:
  - nombre: Iglesias Santandreu, Julieta
    legajo: 73338/3
    mail: juliglesias97@gmail.com

  - nombre: Mancini Perez, Ian Lihuel Demian 
    legajo: 75642/9
    mail: ianmethyst@gmail.com

  - nombre: Pérez Alcolea, Manuel Ignacio
    legajo: 76492/7
    mail: perezalcoleam@gmail.com

  - nombre: Reartes, Santiago Antonio
    legajo: 75662/3
    mail: san.reartes@gmail.com

  - nombre: Szumilo, Dante Andrés
    legajo: 75693/1
    mail: danteszumilo@gmail.com

  - nombre: Valdez Avila, Juan Lautaro 
    legajo: 75570/0
    mail: laurato.valdez.avila@gmail.com

  - nombre: Viggiano, Julián
    mail: julian.viggiano@gmail.com
    legajo: 59921/9


geometry: margin=1in
papersize: a4
toc: true
toc-title: Índice
lang: es-ES
numbersections: true
---
