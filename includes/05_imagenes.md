﻿
\newpage
\appendix

# Imágenes de las piezas sin intervenir

Las siguientes imágenes corresponden a fotografías de las piezas tomadas durante el día del lanzamiento del trabajo práctico.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.40\textwidth]{./img/voltaire.jpg}
  \caption{Busto de Voltaire}
  \label{fig:busto}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.40\textwidth]{./img/pierna_2.jpeg}
  \caption{Pierna de Antinoo}
  \label{fig:pierna}
\end{figure}
