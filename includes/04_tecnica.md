
# Precisiones técnicas

En esta sección hablaremos puntualmente de los diferentes desafíos que tuvimos que superar desde técnica para desarrollar la instalación

## Software

Como por diseño contábamos con al menos tres módulos (mesa, SAR y STD), tuvimos que hacer una selección de tecnologías para desarrollar cada parte individual, además de buscar una forma de inter-comunicarlos, para lograr que la instalación fuera autónoma. A continuación se encuentra un diagrama detallando las tecnologías con las que se construyó cada módulo y los protocolos que se utilizaron para comunicarlos.

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{./img/tech.pdf}
  \caption{Diagrama de módulos y tecnologías}
  \label{fig:tech}
\end{figure}

Para el módulo de la mesa, se utilizó Processing[^processing] en el programa principal que calcula cuándo un puzzle se resolvió, mientras que un programa de Arduino[^arduino] controla el estado de los leds. El módulo de SAR fue desarrollado completamente en Processing, mientras que el de STD está realizado en React.js[^react] para la parte de interfaz y estado, mientras que el renderizado 3D es realizado por three.js[^three].

Finalmente, tuvo que desarrollarse un módulo extra que se encarga de centralizar la información y reenviar mensajes al resto de los módulos, además de funcionar como un lanzador para los demás.

Para comunicar la mayoría de las partes, decidimos usar OSC[^osc] debido a que es el protocolo que ReactIVIsion usa para enviar la información espacial de los distintos objetos. Además, usamos el protocolo Serial[^serial] para para comunicar el programa de la mesa con la placa Arduino, mientras que para el STD, ante la falta de una implementación nativa de OSC para navegadores, se programó un servidor que recibe información por OSC y la reenvía por WebSockets[^websockets].

[^processing]: Sitio web de Processing: [https://processing.org/](https://processing.org/)
[^arduino]: Sitio web de Arduino: [https://arduino.cc/](https://arduino.cc/)
[^react]: Sitio web de React: [https://reactjs.org/](https://reactjs.org/)
[^three]: Sitio web de three.js: [https://threejs.org/](https://threejs.org/)
[^osc]: Sitio web de OSC en Wikipedia en español: [https://es.wikipedia.org/wiki/OpenSound_Control](https://es.wikipedia.org/wiki/OpenSound_Control)
[^serial]: Sitio web de comunicación serie en Wikipedia en español: [https://es.wikipedia.org/wiki/Comunicaci%C3%B3n_serie](https://es.wikipedia.org/wiki/Comunicaci%C3%B3n_serie)
[^websockets]: Sitio web de WebSockets en Wikipedia en español: [https://es.wikipedia.org/wiki/WebSocket](https://es.wikipedia.org/wiki/WebSocket)


## Consideraciones con respecto a la mesa

La mesa es utilizada para contener todo el hardware: la PC, Arduino, leds, router (red WiFI) y el proyector, pero además también contiene una luz infrarroja y una cámara sin filtro infrarrojo para poder realizar el seguimiento de las semiesferas incluso cuando no hay luz visible. Esto fue pensado así para que la instalación funcione en cualquier lugar independientemente de las situaciones de luz.

## Concurrencia

Para poder sincronizar todos los módulos y animaciones, tuvimos que diagramar una linea de tiempo, que nos permitió tener una visión más clara y unifacada (a nivel grupo) de las acciones que ocurrían en cada momento. De haber omitido esto, organizar o hacer un seguimiento de lo que ocurría en cada momento del tiempo hubiese sido una tarea muy compleja y propensa a fallar. Mientras que todos los programas que corren en la PC están sincronizados y conectados todo el tiempo sin interferencias, los celulares al conectarse tienen que solicitar el tiempo actual de la animación para actualizarse y que haya una concurrencia real.

A continuación se encuentra el diagrama que utilizamos para llevar a cabo esta tarea: 

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{./img/timeline.pdf}
  \caption{Diagrama de tiempos de animaciones}
  \label{fig:timeline}
\end{figure}



