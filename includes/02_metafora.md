
# Toma de partido conceptual

La instalación busca describir de forma simbólica cómo el Renacimiento cimentó el camino para la aparición del clima de época: el *zeitgeist*[^1] del Ilustración, abordando el trabajo desde una perspectiva que busque dar cuenta de cómo las ideas propugnadas por Voltaire fueron precursoras de, más tarde, los ideales del período que comenzó con la Revolución Francesa: "libertad, igualdad, fraternidad".

[^1]: Definido como "La actitud general o cualidad de un periodo histórico particular, demostrado así por sus ideas, creencias, etc. comunes en ese momento" (Traducción propia de la definición encontrada en https://www.oxfordlearnersdictionaries.com/definition/english/zeitgeist).

\begin{figure}[H]
  \centering
  \includegraphics[width=0.33\textwidth]{./img/voltaire_tour.jpg}
  \caption{Retrato de Voltaire pintado por Maurice Quentin de La Tour (1735)}
  \label{fig:voltaire_tour}
\end{figure}

Es necesario aclarar que tenemos en cuenta el hecho de que el clima de época del Iluminismo no fue, por supuesto, influenciado por Voltaire solamente, dado que se cuentan además entre sus principales figuras a Montesquieu, Rousseau, Smith, Kant, Hume. Pero se sabe que Voltaire supo encarnar en gran medida el imaginario del siglo de las luces: una fe en el humano y en la capacidad de la razón y la experiencia como instrumentos que permitieran disipar las tinieblas que se asociaban al pasado.

## Asociación: zeitgeist y eidolon

Buscamos hacer una asociación entre dos conceptos: uno es el *zeitgeist*, ya mencionado, y otro es el de *eidolon*. El primero, como se dijo, equivale a "espíritu de tiempo", de uno específico, así probado por su etimología: *zeit* (tiempo) y *geist* (fantasma). Eidolon, por su parte, viene de ${\epsilon}$ ${\iota}$ ${\delta}$ ${\omega}$ ${\lambda}$ o ${\nu}$, que se vincula, en español, a "doble, imagen, fantasma". Así, buscamos representar de forma "corpórea" (es decir, darle una entidad física, mediante un modelo 3D) al espíritu de tiempo del Renacimiento, de forma que ese *eidolon* moldee al "Ilustración" (representado por la cabeza de Voltaire).

El Neoclasicismo convivió con Voltaire, y como una búsqueda artística de retorno a las formas greco-romanas, nos resultó asimismo apropiado el uso de un término de origen Griego.

## Etimología compartida

Es importante, para cimentar la asociación, hacer la siguiente vinculación:

*Eidolon* viene de:

- El Griego *${\epsilon}$ ${\iota}$ ${\delta}$ ${\omega}$ ${\lambda}$ o ${\nu}$* ("fantasma, doble, aparición"), que a su vez deriva de *${\epsilon}$ ${\iota}$ ${\delta}$ o ${\zeta}$* ("imagen, apariencia, forma"), y que a su vez viene del Proto-Indoeuropeo (origen de nuestro idioma y del resto de los idiomas romances) *wéydos* ("imagen"), de weyd- ("ver").

Mientras que Zeitgeist viene de *zeit* ("tiempo") y *geist* ("espíritu"), los cuales a su vez vienen de:

- *Zeit*, del Alto Alemán Medio *zīt*, que viene del Alto Alemán Antiguo *zīt*, que a su vez viene del Proto-Germánico *tīdiz*, y este, del Proto-Indoeuropeo *dīti-* (“time, period”), y, finalmente, este viene de *dī-* (“time”).

- *Geist* viene del Proto-Germánico *gaistaz* (“espíritu”), del Proto-Indoeuropeo *ǵéysd-os*, y este de *ǵéysd-* [^2] (“enojo”).

[^2]: Los dos últimos con una "h" aspirada entre la "ǵ" y "é".

De este recorrido etimológico podemos hacer una asociación común: ambas palabras encuentran su etimología en el Proto-Indoeuropeo, fuente de gran parte de las lenguas actuales del mundo. Se podría argumentar que, paradójicamente, ambas palabras surgieron de un *zeitgeist* común, hace miles de años.
